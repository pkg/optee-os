Source: optee-os
Section: devel
Priority: optional
Maintainer: Dylan Aïssi <daissi@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13),
               device-tree-compiler,
               python3,
               python3-cryptography,
               python3-pyelftools
Vcs-Browser: https://salsa.debian.org/debian/optee-os
Vcs-Git: https://salsa.debian.org/debian/optee-os.git
Homepage: https://www.trustedfirmware.org/projects/op-tee
Rules-Requires-Root: no

Package: optee-os
Architecture: arm64
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: OP-TEE Trusted OS
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the API
 describing how to communicate with a TEE. Those APIs are defined in the
 GlobalPlatform API specifications.

Package: optee-os-dev
Architecture: arm64
Depends:    ${misc:Depends},
            python3
Description: SDK of OP-TEE Trusted OS
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the API
 describing how to communicate with a TEE. Those APIs are defined in the
 GlobalPlatform API specifications.
 .
 The package includes the SDK which is used for TA development.
